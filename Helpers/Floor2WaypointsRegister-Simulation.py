import requests

url = "http://localhost:5000/API/Waypoint"

# Original dimensions (use these to calculate the new scales):
#   Width - 2071
#   Height  - 1395

# new Width - 1800
# new Height - 1334

# Divide the new height and width by the old to calculate the new scaling factors for 

# If changing the values, multiply the new value by (1/scaling factor). For example, if setting Elevators XCoord to 62.2655: 62.2655 × (1÷(widthScale)) = 63.087925553 <- New value (keep the * widthScale)

# widthScale = 0.7735393529 # Full map (rooms included)
# heightScale = 0.9562724014

widthScale = 1
heightScale = 1

requests.put(
    url,
    data = {
        "name" : "Elevators",
        "xCoord" : round(float("54.7952") * float(widthScale), 4),
        "yCoord" : round(float("20.9305") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "0.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "Toilets", # x: 72.1275275856 y: 53.5464710309
        "xCoord" : round(float("62.6631") * float(widthScale), 4),
        "yCoord" : round(float("51.1512") * float(heightScale), 4),
        "directionZ" : "0.0",
        "directionW" : "1.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "Disabled Toilets", # x: 44.4038698876 y: 10.4983154926
        "xCoord" : round(float("38.1367") * float(widthScale), 4),
        "yCoord" : round(float("9.9883") * float(heightScale), 4),
        "directionZ" : "-0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.01 - Nick Efford", # x: 38.1685326894 y: 30.7681727368
        "xCoord" : round(float("32.8651") * float(widthScale), 4),
        "yCoord" : round(float("29.4988") * float(heightScale), 4),
        "directionZ" : "0.0",
        "directionW" : "1.0",
        "waiting" : "False"
    }
)

requests.put(
        url,
    data = {
        "name" : "2.03 - Tutor Room", # x: 37.851364732 y: 35.0959593251
        "xCoord" : round(float("32.8395") * float(widthScale), 4),
        "yCoord" : round(float("33.4431") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "0.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.04 - Post Doctorate Office", # x: 20.7912962123 y: 38.1915335988
        "xCoord" : round(float("17.9727") * float(widthScale), 4),
        "yCoord" : round(float("36.1016") * float(heightScale), 4),
        "directionZ" : "-0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.05 - Teaching Lab", # x: 28.6099043039 y: 38.2648266419
        "xCoord" : round(float("24.7835") * float(widthScale), 4),
        "yCoord" : round(float("40.4936") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.07 - Robotics Lab", # x: 37.9423550591 y: 37.9445149861
        "xCoord" : round(float("32.7524") * float(widthScale), 4),
        "yCoord" : round(float("35.9275") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.08 - Meeting room", # x: 46.9415314056 y: 37.8073673026
        "xCoord" : round(float("41.0727") * float(widthScale), 4),
        "yCoord" : round(float("36.1569") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.10 - Meeting room", # x: 50.4728999522 y: 37.5550553867
        "xCoord" : round(float("43.6665") * float(widthScale), 4),
        "yCoord" : round(float("36.1521") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.12 - Tutor Room", # x: 65.788897638 y: 48.9362812425
        "xCoord" : round(float("59.5396") * float(widthScale), 4),
        "yCoord" : round(float("46.5259") * float(heightScale), 4),
        "directionZ" : "-0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.13 - Tutor Room", # x: 68.7695361528 y: 49.0433883636
        "xCoord" : round(float("59.5396") * float(widthScale), 4),
        "yCoord" : round(float("46.3836") * float(heightScale), 4),
        "directionZ" : "-0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.15 - Teaching Lab", # x: 60.727136659 y: 48.5140735429
        "xCoord" : round(float("57.0962") * float(widthScale), 4),
        "yCoord" : round(float("46.3183") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "0.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.16 - Visualisation Technology", # x: 61.8585858715 y: 48.555053014
        "xCoord" : round(float("53.7067") * float(widthScale), 4),
        "yCoord" : round(float("46.4127") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.17 - Wormery", # x: 68.681997265 y: 48.4846672126
        "xCoord" : round(float("59.4611") * float(widthScale), 4),
        "yCoord" : round(float("46.2743") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.21 - Group Research Area", # x: 72.443782647 y: 48.6282374411
        "xCoord" : round(float("62.6077") * float(widthScale), 4),
        "yCoord" : round(float("46.5049") * float(heightScale), 4),
        "directionZ" : "0.0",
        "directionW" : "1.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.23 - Post Doctorate Office", # x: 80.6173203692 y: 38.0591550467
        "xCoord" : round(float("70.0113") * float(widthScale), 4),
        "yCoord" : round(float("36.0965") * float(heightScale), 4),
        "directionZ" : "-0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.24 - Study Zone", # x: 63.3141502211 y: 35.5507333212
        "xCoord" : round(float("56.8897") * float(widthScale), 4),
        "yCoord" : round(float("34.1469") * float(heightScale), 4),
        "directionZ" : "-0.3",
        "directionW" : "1.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.25 - SOC Admin", # x: 62.9042157737 y: 16.2969598725
        "xCoord" : round(float("54.7581") * float(widthScale), 4),
        "yCoord" : round(float("15.5225") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "0.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.26 - VR Delivery", # x: 70.3309833449 y: 10.3168386477
        "xCoord" : round(float("60.9158") * float(widthScale), 4),
        "yCoord" : round(float("9.7540") * float(heightScale), 4),
        "directionZ" : "0.0",
        "directionW" : "1.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.27 - PhD Office", # x: 64.7710186546 y: 10.683826427
        "xCoord" : round(float("55.7757") * float(widthScale), 4),
        "yCoord" : round(float("9.8457") * float(heightScale), 4),
        "directionZ" : "-0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.28 - PhD Office", # x: 52.2779034464 y: 10.956613042
        "xCoord" : round(float("45.0019") * float(widthScale), 4),
        "yCoord" : round(float("9.9479") * float(heightScale), 4),
        "directionZ" : "-0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.29 - Andy Bullpit", # x: 50.4622296206 y: 10.7033919954
        "xCoord" : round(float("44.0153") * float(widthScale), 4),
        "yCoord" : round(float("10.1142") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.30 - Kitchen", # x: 47.5368180898 y: 10.6986198915
        "xCoord" : round(float("41.3009") * float(widthScale), 4),
        "yCoord" : round(float("9.9547") * float(heightScale), 4),
        "directionZ" : "-0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.33 - Eric Atwell", # x: 42.2623037305 y: 8.06691727305
        "xCoord" : round(float("35.8934") * float(widthScale), 4),
        "yCoord" : round(float("7.6894") * float(heightScale), 4),
        "directionZ" : "-0.5",
        "directionW" : "0.85",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.34 - John Stell", # x: 40.7286664095 y: 7.56170376777
        "xCoord" : round(float("34.9519") * float(widthScale), 4),
        "yCoord" : round(float("7.3940") * float(heightScale), 4),
        "directionZ" : "-0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.35 - Printer", # x: 40.7366508763 y: 8.73550522881
        "xCoord" : round(float("40.7366") * float(widthScale), 4),
        "yCoord" : round(float("8.7355") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "1.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.36 - Post Doctorate Office", # x: 33.4728690575 y: 10.9719422623
        "xCoord" : round(float("34.9519") * float(widthScale), 4),
        "yCoord" : round(float("7.2958") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "0.0",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.37 - Lecture", # x: 31.8738561197 y: 10.2487270545
        "xCoord" : round(float("27.9743") * float(widthScale), 4),
        "yCoord" : round(float("10.0315") * float(heightScale), 4),
        "directionZ" : "0.9",
        "directionW" : "0.4",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.39 - Sam Wilson", # x: 45.9419421008 y: 10.4876640529
        "xCoord" : round(float("39.8096") * float(widthScale), 4),
        "yCoord" : round(float("9.9106") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Andy Office 1", # x: 52.7707950622 y: 11.4245403797
        "xCoord" : round(float("45.7031") * float(widthScale), 4),
        "yCoord" : round(float("10.1474") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Andy Office 2", # x: 54.8394841179 y: 11.359233614
        "xCoord" : round(float("48.1298") * float(widthScale), 4),
        "yCoord" : round(float("10.2419") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Andy Office 3", # x: 57.7307652277 y: 11.1777576691
        "xCoord" : round(float("51.5468") * float(widthScale), 4),
        "yCoord" : round(float("10.2851") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Robotics Lab 1", # x: 39.4519455931 y: 38.2048175785 
        "xCoord" : round(float("34.1136") * float(widthScale), 4),
        "yCoord" : round(float("36.1673") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Robotics Lab 2", # x: 42.0248720849 y: 38.0955597553
        "xCoord" : round(float("36.6886") * float(widthScale), 4),
        "yCoord" : round(float("36.1130") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Toilets 1", # x: 71.4492022341 y: 51.1355379362
        "xCoord" : round(float("62.4137") * float(widthScale), 4),
        "yCoord" : round(float("48.9410") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "0.0",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Toilets 2", # x: 71.3537298443 y: 53.5641256764
        "xCoord" : round(float("62.5712") * float(widthScale), 4),
        "yCoord" : round(float("53.7938") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "0.0",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Elevator 1", # x: 62.0002443476 y: 18.5993895864
        "xCoord" : round(float("54.4816") * float(widthScale), 4),
        "yCoord" : round(float("17.3612") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "0.0",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Elevator 2", # x: 62.183718637 y: 26.8848186916 
        "xCoord" : round(float("54.6952") * float(widthScale), 4),
        "yCoord" : round(float("25.7794") * float(heightScale), 4),
        "directionZ" : "1.0",
        "directionW" : "0.0",
        "waiting" : "True"
    }
)

print(requests.put(
    url,
    data = {
        "name" : "Waiting - Teaching Lab", # x: 59.1186249115 y: 49.115591027 
        "xCoord" : round(float("55.5257") * float(widthScale), 4),
        "yCoord" : round(float("46.4777") * float(heightScale), 4),
        "directionZ" : "0.7",
        "directionW" : "0.7",
        "waiting" : "True"
    }
))

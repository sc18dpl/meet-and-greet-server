import requests

url = "http://localhost:5000/API/Waypoint"

requests.put(
    url,
    data = {
        "name" : "Elevators", # x: 63.0879372538 y: 22.9272293248
        "xCoord" : "63.0879",
        "yCoord" : "22.9272",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "Toilets", # x: 72.1275275856 y: 53.5464710309
        "xCoord" : "72.1275",
        "yCoord" : "53.5464",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "Disabled Toilets", # x: 44.4038698876 y: 10.4983154926
        "xCoord" : "44.4038",
        "yCoord" : "10.4983",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.01 - Nick Efford", # x: 38.1685326894 y: 30.7681727368
        "xCoord" : "38.1685",
        "yCoord" : "30.7681",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.03 - Tutor Room", # x: 37.851364732 y: 35.0959593251
        "xCoord" : "37.8513",
        "yCoord" : "35.0959",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.04 - Post Doctorate Office", # x: 20.7912962123 y: 38.1915335988
        "xCoord" : "20.7912",
        "yCoord" : "38.1915",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.05 - Teaching Lab", # x: 28.6099043039 y: 38.2648266419
        "xCoord" : "28.6099",
        "yCoord" : "38.2648",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.07 - Robotics Lab", # x: 37.9423550591 y: 37.9445149861
        "xCoord" : "37.9423",
        "yCoord" : "37.9445",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.08 - Meeting room", # x: 46.9415314056 y: 37.8073673026
        "xCoord" : "46.9415",
        "yCoord" : "37.8073",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.10 - Meeting room", # x: 50.4728999522 y: 37.5550553867
        "xCoord" : "50.4728",
        "yCoord" : "37.5550",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.12 - Tutor Room", # x: 65.788897638 y: 48.9362812425
        "xCoord" : "65.7888",
        "yCoord" : "48.9362",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.13 - Tutor Room", # x: 68.7695361528 y: 49.0433883636
        "xCoord" : "68.7695",
        "yCoord" : "49.0433",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.15 - Teaching Lab", # x: 60.727136659 y: 48.5140735429
        "xCoord" : "60.7271",
        "yCoord" : "48.5140",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.16 - Visualisation Technology", # x: 61.8585858715 y: 48.555053014
        "xCoord" : "61.8585",
        "yCoord" : "48.5550",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.17 - Wormery", # x: 68.681997265 y: 48.4846672126
        "xCoord" : "68.6819",
        "yCoord" : "48.4846",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.21 - Group Research Area", # x: 72.443782647 y: 48.6282374411
        "xCoord" : "72.4437",
        "yCoord" : "48.6282",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.23 - Post Doctorate Office", # x: 80.6173203692 y: 38.0591550467
        "xCoord" : "80.6173",
        "yCoord" : "38.0591",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.24 - Study Zone", # x: 63.3141502211 y: 35.5507333212
        "xCoord" : "63.3141",
        "yCoord" : "35.5507",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.25 - SOC Admin", # x: 62.9042157737 y: 16.2969598725
        "xCoord" : "62.9042",
        "yCoord" : "16.2969",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.26 - VR Delivery", # x: 70.3309833449 y: 10.3168386477
        "xCoord" : "70.3309",
        "yCoord" : "10.3168",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.27 - PhD Office", # x: 64.7710186546 y: 10.683826427
        "xCoord" : "64.7710",
        "yCoord" : "10.6838",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.28 - PhD Office", # x: 52.2779034464 y: 10.956613042
        "xCoord" : "52.2779",
        "yCoord" : "10.9566",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.29 - Andy Bullpit", # x: 50.4622296206 y: 10.7033919954
        "xCoord" : "50.4622",
        "yCoord" : "10.7033",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.30 - Kitchen", # x: 47.5368180898 y: 10.6986198915
        "xCoord" : "47.5368",
        "yCoord" : "10.6986",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.33 - Eric Atwell", # x: 42.2623037305 y: 8.06691727305
        "xCoord" : "42.2623",
        "yCoord" : "8.0669",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.34 - John Stell", # x: 40.7286664095 y: 7.56170376777
        "xCoord" : "40.7286",
        "yCoord" : "7.5617",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.35 - Printer", # x: 40.7366508763 y: 8.73550522881
        "xCoord" : "40.7366",
        "yCoord" : "8.7355",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.36 - Post Doctorate Office", # x: 33.4728690575 y: 10.9719422623
        "xCoord" : "33.4728",
        "yCoord" : "10.9719",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.37 - Lecture", # x: 31.8738561197 y: 10.2487270545
        "xCoord" : "31.8738",
        "yCoord" : "10.2487",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "2.39 - Sam Wilson", # x: 45.9419421008 y: 10.4876640529
        "xCoord" : "45.9419",
        "yCoord" : "10.4876",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Andy Office 1", # x: 52.7707950622 y: 11.4245403797
        "xCoord" : "52.7707",
        "yCoord" : "11.4245",
        "direction" : 
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Andy Office 2", # x: 54.8394841179 y: 11.359233614
        "xCoord" : "54.8394",
        "yCoord" : "11.3592",
        "direction" : 
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Andy Office 3", # x: 57.7307652277 y: 11.1777576691
        "xCoord" : "57.7307",
        "yCoord" : "11.1777",
        "direction" : 
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Robotics Lab 1", # x: 39.4519455931 y: 38.2048175785 
        "xCoord" : "39.4519",
        "yCoord" : "38.2048",
        "direction" : 
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Robotics Lab 2", # x: 42.0248720849 y: 38.0955597553
        "xCoord" : "42.0248",
        "yCoord" : "38.0955",
        "direction" : 
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Toilets 1", # x: 71.4492022341 y: 51.1355379362
        "xCoord" : "71.4492",
        "yCoord" : "51.1355",
        "direction" : 
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Toilets 2", # x: 71.3537298443 y: 53.5641256764
        "xCoord" : "71.3537",
        "yCoord" : "53.5641",
        "direction" : 
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Elevator 1", # x: 62.0002443476 y: 18.5993895864
        "xCoord" : "62.0002",
        "yCoord" : "18.5993",
        "direction" : 
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Elevator 2", # x: 62.183718637 y: 26.8848186916 
        "xCoord" : "62.1837",
        "yCoord" : "26.8848",
        "direction" : 
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Waiting - Teaching Lab", # x: 59.1186249115 y: 49.115591027 
        "xCoord" : "59.1186",
        "yCoord" : "49.1155",
        "direction" : 
        "waiting" : "True"
    }
)

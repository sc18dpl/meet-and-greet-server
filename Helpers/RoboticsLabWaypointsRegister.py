import requests

url = "http://localhost:5000/API/Waypoint"

requests.put(
    url,
    data = {
        "name" : "Front Door",
        "xCoord" : "-2.874",
        "yCoord" : "-11.612",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "Cabinet Corner",
        "xCoord" : "-5.772",
        "yCoord" : "-6.722",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "Lab Door",
        "xCoord" : "1.87",
        "yCoord" : "-1.94",
        "direction" : "1",
        "waiting" : "False"
    }
)

requests.put(
    url,
    data = {
        "name" : "Front Door - Waiting",
        "xCoord" : "0",
        "yCoord" : "-11.8",
        "direction" : "0",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Cabinet Corner - Waiting",
        "xCoord" : "-4.67",
        "yCoord" : "-7.67",
        "direction" : "-0.7",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Lab Window Corner - Waiting",
        "xCoord" : "0.683",
        "yCoord" : "1.121",
        "direction" : "0",
        "waiting" : "True"
    }
)

requests.put(
    url,
    data = {
        "name" : "Wall Window Corner - Waiting",
        "xCoord" : "-4.799",
        "yCoord" : "2.83",
        "direction" : "1",
        "waiting" : "True"
    }
)

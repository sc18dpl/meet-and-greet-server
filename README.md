# Meet And Greet - Server

A Flask server to handle scheduling of multiple turtlebots in a "meet and greet" environment.

## Requirements

Python3.6 (or greater).

The packages defined by requirements.txt are installed.

ROS melodic (or newer) is installed on the system. This can be tested by attempting to import rospy within a python3 instance.

Either the "Turtlebot" or "Turtlebot3" ROS packages should be correctly installed within the ROS melodic instance.

## Execution

Download and unpack the package.

cd to the source code root directory.

`flask run` - To execute the server on an IP address only accessable to the host machine.

`flask run --host=0.0.0.0` - To execute the server on an IP address accessable to all machines networked to the host.

## Waypoint management

Within the "/helpers" directory, a number of example waypoint declaration scripts are included.

To update the waypoints stored within the server, it is recommended to delete the "app.db" file within the root directory, followed by running `python3 db_create.py`. After which a waypoints declaration python script can be executed (assuming the server is running).

Any waypoints added should be immediatly visible within the monitor page.



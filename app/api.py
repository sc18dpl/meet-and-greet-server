import sys
from flask import Flask, request, jsonify, make_response, jsonify
from flask_restful import Resource, Api, reqparse

import rospy
from nav_msgs.srv import GetPlan # Necessary for real world distance tracking
from geometry_msgs.msg import PoseStamped

from app import db, app, models

from datetime import datetime, timedelta
import atexit

import requests

import math

from apscheduler.schedulers.background import BackgroundScheduler 

import six
import sys
sys.modules["sklearn.externals.six"] = six # Janky workaround to fix dependancy issue with mlrose
import mlrose
import numpy as np

api = Api(app)

WaypointDistances = None

ClientRequestQueue = [] # Pre-prepared client request JSON data will be appended to this list when requests should be queued up

URL = None


def getRealWorldDistance(startX, startY, goalX, goalY):
	start = PoseStamped()
	start.header.frame_id = "map"
	start.pose.position.x = float(startX)
	start.pose.position.y = float(startY)
	start.pose.orientation.w = 1

	goal = PoseStamped()
	goal.header.frame_id = "map"
	goal.pose.position.x = float(goalX)
	goal.pose.position.y = float(goalY)
	goal.pose.orientation.w = 1

	tolerence = 0.5

	# For this to work, either an empty gazebo world of Turtlebot3 or Turtlebot2, and move_base + AMCL must be running!
	# Bonus points for placing the turtlebot out of the actual map area so its readings can't interfear with the map data

	# roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch
	# roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=<Path to map file>

	# roslaunch turtlebot_gazebo turtlebot_world.launch
	# roslaunch turtlebot_gazebo amcl_demo.launch map_file:=<Path to map file>

	try:
		get_plan = rospy.ServiceProxy("/move_base/make_plan", GetPlan) # Proxy our service call through make_plan

		resp = get_plan(start, goal, tolerence)
		data = resp.plan.poses # Perform the pathfinding

		if len(data) == 0:
			return -1 # Invalid coordinates, or no path found

		runningDistance = 0

		for count in range(0, len(data) -1):
			startPoint = data[count].pose.position # Grab the target point and the next point in the path
			endPoint = data[count +1].pose.position

			distance = math.sqrt(pow(startPoint.x - endPoint.x, 2) + pow(startPoint.y - endPoint.y, 2))

			runningDistance = runningDistance + distance

		return runningDistance

	except: # Can't get real world data, so just default to as the crow flies distance
		return math.sqrt(pow(startX - goalX, 2) + pow(startY - goalY, 2))

def checkBotState():
	# https://stackoverflow.com/questions/21214270/how-to-schedule-a-function-to-run-every-hour-on-flask

	timeoutThreshold = 60 # How many seconds should a bot have not pinged for to be considered inactive
	staleLoopThreshold = 10 # How many loops does a bot need to be stale for before we wipe it from the DB?

	robots = models.Robot.query.all() # Grab all robots

	if (robots is not None):
		for robot in robots:
			lastPing = robot.lastPing

			delta = datetime.now() - lastPing

			if delta.seconds > timeoutThreshold:
				robot.stale = True
				robot.staleCount = robot.staleCount + 1

				if robot.staleCount > staleLoopThreshold: # Robot has been gone for too long, lets just remove it from the server. It can re-register
					db.session.delete(robot)
					db.session.commit()

					return

			else:
				robot.stale = False

			if (robot.timeout != None): # Error handling - timeout should only ever be none when a robot isn't assigned a client
				if ((robot.state == "waiting") and (robot.timeout < datetime.now())): # Robot has waited x minutes for their client. Return to the home state and clear their data
					robot.state = "home"
					robot.timeout = None
					robot.waypointName = "home"
					robot.targetWaypointList = None
				
			db.session.add(robot)
			db.session.commit()

	if len(ClientRequestQueue) > 0: # Theres something in the queue, lets try and request a bot to that client
		global URL

		url = str(URL) + str("API/Client")
		response = requests.get(url, params=ClientRequestQueue[0])

		if (response.status_code == 200):
			ClientRequestQueue.pop(0) # The request was successful, so lets remove the JSON data

def calculateDistances(crashed=False):
	waypointDistances = {}

	if (crashed == False): # Were not running db_create
		# Build a data structure of distances between all waypoints

		waypoints = models.Waypoints.query.all() # Grab all the waypoints

		for waypoint in waypoints:
			waypointDistances[waypoint.name] = {} # Initialise our dictionary object

			startX = waypoint.xCoordinate
			startY = waypoint.yCoordinate

			for secondWaypoint in waypoints: # Re-itterate over the waypoints, and calculate the distance from our start point to all other waypoints
				goalX = secondWaypoint.xCoordinate
				goalY = secondWaypoint.yCoordinate

				distance = getRealWorldDistance(startX, startY, goalX, goalY) # Calculate the distance. Real World if available

				waypointDistances[waypoint.name][secondWaypoint.name] = distance # Save this distance to our data structure

	global WaypointDistances
	WaypointDistances = waypointDistances

def initialisation():
	scheduler = BackgroundScheduler(timezone="Europe/London")
	scheduler.add_job(func=checkBotState, trigger="interval", seconds=5) # Initialise the checkBotState function every 5 seconds
	scheduler.start()
	atexit.register(lambda: scheduler.shutdown())

	crashed = False

	try:
		# When the server spins-up, we should remove any robots that are currently stored in the DB as they should "check in" on spin up
		robots = models.Robot.query.all() # Grab all robots

		for robot in robots:
			db.session.delete(robot)
		
		db.session.commit()
	except:
		print ("Clearing robots crashed, likely a result of db_create")
		crashed = True

	try:
		# When the server spins-up, we should remove any waypoints starting with __ as these are temporary client waypoints
		waypoints = models.Waypoints.query.all()

		for waypoint in waypoints:
			if waypoint.name[:2] == "__":
				db.session.delete(waypoint)
			
			db.session.commit()
	except:
		print ("Clearing temporary waypoints crashed, likely a result of db_create")
		crashed = True

	calculateDistances(crashed)


class ClientRequest(Resource): # User makes a bot request, this endpoint should handle it
	def checkAvaibilityOfMakePlan(self):
		try:
			get_plan = rospy.ServiceProxy("/move_base/make_plan", GetPlan) # Proxy our service call through make_plan
			return True

		except:
			return False

	def get(self):

		# Return Codes
		# 200 - Successfully scheduled bot, check response JSON for details
		
		# 400 - No bots available
		# 401 - Invalid Waypoint
		# 402 - Another bot is scheduled to the clients location

		# Technically this should be a Put request, and I might change it in the future, but for now its nice to 
		# make requests from a browser

		parser = reqparse.RequestParser()
		parser.add_argument("clientXCoord", type=float, required=True, help="What is your xCoord") # Whats the clients xCoordinate
		parser.add_argument("clientYCoord", type=float, required=True, help="What is your yCoord") # Whats the clients xCoordinate
		parser.add_argument("clientName", type=str, required=True, help="What is the name of the client?")

		# Atleast 1 waypoint is required
		parser.add_argument("targetWaypointName1", type=str, required=True, help="What waypoint has the client requested?") # How many records should we return?

		argumentCount = 100

		# The other x are optional
		for i in range(2, argumentCount):
			parser.add_argument("targetWaypointName" + str(i), type=str, required=False)
		
		args = parser.parse_args()

		targetWaypoints = []

		for i in range(1, argumentCount):
			if (args["targetWaypointName" + str(i)] != None and args["targetWaypointName" + str(i)] not in targetWaypoints): # See if a waypoint has been requested, and ensure its not a duplicate
				targetWaypoints.append(args["targetWaypointName" + str(i)])

		clientXCoord = args["clientXCoord"]
		clientYCoord = args["clientYCoord"]
		clientName = args["clientName"]

		global WaypointDistances # Solve the TSP to find the best route between all passed waypoints

		if len(targetWaypoints) > 1: # We should only perform TSP if there is more than 1 waypoint
			targetDistances = []
			mapping = {}
			inverseMapping = {}

			for i in range(0, len(targetWaypoints)):
				mapping[targetWaypoints[i]] = i
				inverseMapping[i] = targetWaypoints[i]

			for waypointName in targetWaypoints:
				waypoint = models.Waypoints.query.filter_by(name=waypointName).first()
				for tempWaypointName in targetWaypoints:
					tempWaypoint = models.Waypoints.query.filter_by(name=tempWaypointName).first()

					if WaypointDistances[waypoint.name][tempWaypoint.name] > 0:
						targetDistances.append((mapping[waypoint.name], mapping[tempWaypoint.name], WaypointDistances[waypoint.name][tempWaypoint.name]))

			fitness_dists = mlrose.TravellingSales(distances = targetDistances)
			
			problem_fit = mlrose.TSPOpt(
				length = len(targetWaypoints),
				fitness_fn = fitness_dists,
				maximize = False
			)

			best_state, best_fitness = mlrose.genetic_alg(problem_fit, random_state = 2)

			resultingOrder = []

			for i in range(0, len(best_state)):
				resultingOrder.append(inverseMapping[best_state[i]])

			targetWaypoints = resultingOrder

		# Now a loop has been established, we need to find the point closest to the Client location and re-order the loop to "start" there

			currentClosest = None
			currentClosestDistance = None

			for waypoint in targetWaypoints:
				waypointTemp = models.Waypoints.query.filter_by(name = waypoint).first()

				distance = getRealWorldDistance(
					clientXCoord,
					clientYCoord,
					waypointTemp.xCoordinate,
					waypointTemp.yCoordinate
				)

				if currentClosest == None or distance < currentClosestDistance:
					currentClosest = waypoint
					currentClosestDistance = distance

			index = targetWaypoints.index(currentClosest)

			for i in range(0, index):
				targetWaypoints.append(targetWaypoints[i])

			for i in range(0, index): # This has to be done in seperate loops to avoid indexing errors
				targetWaypoints.pop(0) # Pop the first location, index times

			print ("The best pathing (Starting at client location: " + str(clientXCoord) + ", " + str(clientYCoord) + ") is: " + str(targetWaypoints) + ", with a distance of: " + str(best_fitness))


		# Get all available bots 
		robots = models.Robot.query.filter_by(state = "available", stale = False).all()

		for waypointName in targetWaypoints:
			waypoint = models.Waypoints.query.filter_by(name = waypointName).first() # Check the waypointName is valid

			if (waypoint is None):
				return make_response("Invalid waypoint: " + str(waypointName), 401) # 401 for invalid waypoint

		if len(robots) > 0:
			targetRobot = None
			currentBest = None

			if self.checkAvaibilityOfMakePlan() == False:
				print ("make_plan unavailable. Using straight line distances to schedule bots!")

			# Find the closest bot to the client

			for robot in robots:
				distanceToClient = getRealWorldDistance( # Calculate the real world distance to the client
					robot.xCoordinate,
					robot.yCoordinate,
					clientXCoord,
					clientYCoord
				)

				if distanceToClient == -1: # No path from bot to client
					continue # Skip to the next bot to see if they can make it or not

				else: # valid path to target
					if currentBest == None:
						currentBest = distanceToClient # we have a new prefered bot
						targetRobot = robot
					
					else:
						if distanceToClient < currentBest:
							currentBest = distanceToClient # we have a new prefered bot
							targetRobot = robot

			if targetRobot == None:
				return make_response("No available bots!", 400) # There are no available bots that can make it to the client

			testWaypoints = models.Robot.query.filter_by(xCoordinate=clientXCoord, yCoordinate=clientYCoord).all() # For now, only allow 1 bot to be scheduled to a client location
			for testWaypoint in testWaypoints:
				testRobot = models.Robot.query.filter_by(waypointName=testWaypoint.name).first()
				
				if testRobot is not None:
					return make_response("A bot is already scheduled to your location! Please wait for that bot to leave", 402)

			clientWaypoint = models.Waypoints( # Add the client waypoint for the bot to travel to 
				name = "__" + str(targetRobot.name) + "_Waypoint__",
				xCoordinate = clientXCoord, 
				yCoordinate = clientYCoord,
				waiting = False,
				directionZ = 0.7,
				directionW = 0.7
			)

			db.session.add(clientWaypoint) # Add the client target waypoint

			targetRobot.state = "client" # Bot should travel to the clients location
			targetRobot.targetWaypointList = targetWaypoints
			targetRobot.waypointName = clientWaypoint.name
			targetRobot.clientName = clientName
			
			db.session.add(targetRobot)
			db.session.commit()

			print("Client request made at waypointName: " + str(waypoint.name) + ". " + targetRobot.name + " assigned!")
			print(targetRobot)

			jsonData = jsonify(
				robotName = targetRobot.name,
				robotModel = targetRobot.model,
				robotID = targetRobot.robotID,
				timeout = targetRobot.timeout,
			)

			return make_response(jsonData, 200)

		else:
			return make_response("No available bots!", 400)

	def put(self):
		parser = reqparse.RequestParser()
		parser.add_argument("robotID", type=int, required=True, help="What robot is making the request?") # How many records should we return?
		parser.add_argument("client", type=bool, required=True, help="Are you waiting at the clients location (True), or a target waypoint (False)")
		args = parser.parse_args()

		robotID = args["robotID"]
		client = args["client"]

		robot = models.Robot.query.filter_by(robotID = robotID).first()

		if (robot.waypointName[:2] == "__"): # Temporary client waypoint
			waypoint = models.Waypoints.query.filter_by(name=robot.waypointName).first() # Grab the temporary waypoint object to delete it later
			db.session.delete(waypoint) # Were done with this temp waypoint, so lets get rid of it

		targetWaypointList = robot.targetWaypointList

		waypointsList = []

		if len(targetWaypointList) > 0: # There is atleast 1 more waypoint to travel to
			waypointsList = robot.targetWaypointList

			robot.waypointName = waypointsList[0] # Switch the client and target waypoints

			waypointsList.pop(0) # Remove this waypoint from the list, and re-add the waypointList to the db
			newWaypointList = waypointsList # Janky workaround for PickleType object

			robot.targetWaypointList = newWaypointList

			robot.state = "waiting" # Waiting for a button press

			robot.timeout = datetime.now() + timedelta(minutes = 1) 
		
		else:
			robot.state = "home" # All waypoints have been executed, send the bot to a waiting zone

		db.session.add(robot)
		db.session.commit()

		jsonData = jsonify(
			remaining = len(waypointsList) # How many waypoints remaining
		)

		return make_response(jsonData, 200)

class RegisterBot(Resource): # Bot spins up, this endpoint should be called automatically to register the bot to the network. Without this, the bot will do nothing
	def get(self): # Return the active state of the bot. The client can use this to see if the bot should be re-registered
		parser = reqparse.RequestParser()
		parser.add_argument("name", type=str, required=True, help="What robot is this?") # How many records should we return?
		parser.add_argument("robotID", type=int, required=True, help="What model is this bot?") # How many records should we return?
		args = parser.parse_args()

		name = args["name"]
		robotID = args["robotID"]

		robot = models.Robot.query.filter_by(name=name, robotID=robotID).first()

		if robot is None:
			return make_response("Robot is not in server", 200)
		else:
			return make_response("Robot is active in server", 201)
	
	def put(self):
		parser = reqparse.RequestParser()
		parser.add_argument("name", type=str, required=True, help="What robot is this?") # How many records should we return?
		parser.add_argument("model", type=int, required=True, help="What model is this bot?") # How many records should we return?
		args = parser.parse_args()

		name = args["name"]
		model = args["model"]

		robot = models.Robot.query.filter_by(name=name, model=model).first() # Lets see if this bot has already been initialised or not

		if robot is not None: # This bot is already in the database. Proabably went stale. So lets reinitialise it
			robot.stale = False
			robot.ping = datetime.now()

			db.session.add(robot)
			db.session.commit()

			return make_response(str(robot.robotID), 201) # 201 for bot reinitialised

		else: # This is a new bot to the network. So lets initialise it
			robot = models.Robot(
				name = name,
				model = model,
				lastPing = datetime.now(),
				stale = False,
				staleCount = 0,
				state = "home", # The bot should go to home when initialised
				timeout = None,
				waypointName = "home",
				xCoordinate = 0,
				yCoordinate = 0
			)

			db.session.add(robot)
			db.session.commit()

		return make_response(str(robot.robotID), 200) # 200 for new bot added

	def delete(self): # When the bot spins down, it should make a call to remove itself from the active list
		parser = reqparse.RequestParser()
		parser.add_argument("name", type=str, required=True, help="What robot is this?") # How many records should we return?
		parser.add_argument("model", type=int, required=True, help="What model is this bot?") # How many records should we return?
		args = parser.parse_args()

		name = args["name"]
		model = args["model"]

		robot = models.Robot.query.filter_by(name=name, model=model).first()

		if (robot is not None):
			db.session.delete(robot)
			db.session.commit()

			return make_response("", 200)
		
		else:
			return make_response("Invalid bot", 400)

class BotState(Resource):
	def get(self):
		parser = reqparse.RequestParser()
		parser.add_argument("robotID", type=int, required=True, help="What robot is this?") # How many records should we return?
		args = parser.parse_args()

		robotID = args["robotID"]

		robot = models.Robot.query.filter_by(robotID = robotID).first()

		if (robot is None):
			return make_response("Bot unregistered", 400)
		
		else:
			waypoint = models.Waypoints.query.filter_by(name = robot.waypointName).first()

			jsonString = None

			if (waypoint is None): # The robot is currently assigned to an invalid waypoint. This isn't good, so lets just tell the bot to hold position until a better command is issued
				jsonString = '{"state" : "' + str(robot.state) + '", "xCoord" : "X", "yCoord" : "X", "waypointName" : "N/A", "direction" : "N/A"}'	
				response = make_response(jsonString, 201) # 201 for invalid waypoint data

			else:
				jsonString = '{"state" : "' + str(robot.state) + '", "xCoord" : "' + str(waypoint.xCoordinate) + '", "yCoord" : "' + str(waypoint.yCoordinate) + '", "waypointName" : "' + str(waypoint.name) + '", "directionZ" : "' + str(waypoint.directionZ) + '", "directionW" : "' + str(waypoint.directionW) + '"}'
				response = make_response(jsonString, 200) # 200 for valid waypoint data
				
			response.mimetype = "text/plain"
			return response

	def put(self): # Allow the bot to update its state. Example: update "travelling" to "arrived" letting the server know what the bot is doing
		parser = reqparse.RequestParser()
		parser.add_argument("robotID", type=int, required=True, help="What robot is this?") # How many records should we return?
		parser.add_argument("state", type=str, required=True, help="What state should the bot transition to?") # How many records should we return?
		parser.add_argument("lastWaypoint", type=str, required=False)
		args = parser.parse_args()

		robotID = args["robotID"]
		state = args["state"]

		robot = models.Robot.query.filter_by(robotID = robotID).first()

		if (robot is None):
			return make_response("Invalid robotID", 400)

		robot.state = state

		if ((state == "available") or (state == "home") or (state == "travelling")): # If the traisition is from "waiting" to "travelling" or "available" or "home", we should clear the navigation data and just set it to home. Can't go wrong
			robot.timeout = None

		if (state == "lost - Client"): # Robot has failed to localise before moving to client location!! Schedule a new bot ASAP!
			
			print (str(robotID) + " is lost!! Need to reschedule a bot!!")

			robot.state = "lost" # Get rid of the - Client part so the bot can attempt to relocalise
			robot.timeout = None

			clientWaypoint = robot.waypointName

			robot.waypointName = args["lastWaypoint"] # Set their location back to that waiting point to avoid other bots being sent there

			waypoints = robot.targetWaypointList
			clientName = robot.clientName # Extract the required data for the next client request

			robot.clientName = None
			robot.targetWaypointList = None

			db.session.add(robot) # This will be committed later when the client waypoint is deleted

			# Delete the temporary waypoint created for the bot
			item = models.Waypoints.query.filter_by(name=clientWaypoint).first()

			clientX = item.xCoordinate
			clientY = item.yCoordinate # extract all the relevant information to make another client request

			db.session.delete(item) # Delete the client waypoint
			db.session.commit()
			
			jsonData = {}

			jsonData["clientXCoord"] = clientX
			jsonData["clientYCoord"] = clientY
			jsonData["clientName"] = clientName
			jsonData["targetWaypointName1"] = waypoints[0] # The first waypoint in the list

			waypoints.pop(0)

			count = 2
			for waypoint in waypoints:
				jsonData["targetWaypointName" + str(count)] = waypoint
				count += 1
			
			url = str(request.host_url) + str("API/Client")

			global URL # Update this so we have it when we need it
			URL = str(request.host_url)

			response = requests.get(url, params=jsonData)

			if (response.status_code != 200):
				print ("unfortunatly no bots are currently available. Adding to Client queue")

				ClientRequestQueue.append(jsonData) # This will be sorted as soon as a bot is available!

		db.session.add(robot)
		db.session.commit()

		return make_response("State changed successfully", 200)

class Ping(Resource):
	def put(self):
		parser = reqparse.RequestParser()
		parser.add_argument("robotID", type=int, required=True, help="What robot is this?") # How many records should we return?
		parser.add_argument("xCoordinate", type=float, required=True, help="Whats your X Coordinate?") # How many records should we return?
		parser.add_argument("yCoordinate", type=float, required=True, help="Whats your Y coordinate?") # How many records should we return?
		args = parser.parse_args()

		robotID = args["robotID"]
		xCoord = args["xCoordinate"]
		yCoord = args["yCoordinate"]

		robot = models.Robot.query.filter_by(robotID = robotID).first()

		if (robot == None):
			return make_response("Invalid robotID - " + str(datetime.now()), 403)

		robot.lastPing = datetime.now()
		robot.stale = False
		robot.staleCount = 0

		robot.xCoordinate = xCoord
		robot.yCoordinate = yCoord

		db.session.add(robot)
		db.session.commit()

		return make_response("Pong - " + str(datetime.now()), 200) # return a success

class Waypoint(Resource):
	def put(self): # Add a waypoint to the database
		parser = reqparse.RequestParser()
		parser.add_argument("name", type=str, required=True, help="What should the waypoint be named?") # How many records should we return?
		parser.add_argument("xCoord", type=float, required=True, help="What is the X-Coordinate?")
		parser.add_argument("yCoord", type=float, required=True, help="What is the Y-Coordinate?")
		parser.add_argument("directionZ", type=float, required=True, help="What direction should the bot face at the waypoint?")
		parser.add_argument("directionW", type=float, required=True, help="What direction should the bot face at the waypoint?")
		parser.add_argument("waiting", type=str, required=False)
		args = parser.parse_args()

		name = args["name"]
		xCoord = args["xCoord"]
		yCoord = args["yCoord"]
		directionZ = args["directionZ"]
		directionW = args["directionW"]

		waiting = None
		try:
			waiting = args["waiting"]
			if waiting == "True":
				waiting = True
			else:
				waiting = False
		except:
			waiting = False

		items = models.Waypoints.query.filter_by(name=name).first()

		if items is not None:
			return make_response("Waypoint with that name already exists", 400)
		
		waypoint = models.Waypoints(
			name = name,
			xCoordinate = xCoord,
			yCoordinate = yCoord,
			waiting = waiting,
			directionZ = directionZ,
			directionW = directionW
		)

		db.session.add(waypoint)
		db.session.commit()

		calculateDistances() # We need to recalculate our distance matrix incase this waypoint is used during this execution

		return make_response("Waypoint created", 201)

	def delete(self): # Delete a waypoint from the database
		parser = reqparse.RequestParser()
		parser.add_argument("name", type=str, required=True, help="What waypoint name should I delete?") # How many records should we return?
		args = parser.parse_args()

		name = args["name"]

		item = models.Waypoints.query.filter_by(name=name).first()

		if item is not None:
			db.session.delete(item)
			db.session.commit()

			return make_response("Deleted", 200)

		else:
			return make_response("No waypoint with that name", 400)

	def get(self):
		parser = reqparse.RequestParser()
		parser.add_argument("name", type=str, required=True, help="What waypoint should I search for?") # How many records should we return?
		args = parser.parse_args()

		name = args["name"]

		data = models.Waypoints.query.filter_by(name=name).first()

		if data is not None:
			jsonData = jsonify(
				id = data.waypointID,
				name = name,
				xCoord = data.xCoordinate,
				yCoord = data.yCoordinate,
				direction = data.direction
			)

			return make_response(jsonData, 200)

		else:
			return make_response("Invalid waypoint name", 401)

class WaitingWaypoint(Resource):
	def get(self):
		parser = reqparse.RequestParser()
		parser.add_argument("robotID", type=int, required=True, help="What bot are you?") # What bot is making the request
		parser.add_argument("xCoord", type=float, required=True, help="Whats your X coord?") 
		parser.add_argument("yCoord", type=float, required=True, help="Whats your Y coord?")
		args = parser.parse_args()

		# This function should identify the nearest "waiting"
		# waypoint to the bots current location
		# and return it to the bot

		robotID = args["robotID"]
		robot = models.Robot.query.filter_by(robotID = robotID).first()

		waitingWaypoints = models.Waypoints.query.filter_by(waiting=True).all()

		xCoord = args["xCoord"]
		yCoord = args["yCoord"]

		currentClosest = None
		currentClosestDistance = None

		for waypoint in waitingWaypoints:

			# We need to check if the waiting position is taken or not
			testRobot = models.Robot.query.filter_by(waypointName = waypoint.name).first()

			if testRobot is None:
				distance = getRealWorldDistance(
					waypoint.xCoordinate,
					waypoint.yCoordinate,
					xCoord,
					yCoord
				)

				if distance < 0: # We don't want a negative distance. This shouldn't happen, but doesn't hurt
					distance = distance * -1

				if (currentClosestDistance == None) or (distance < currentClosestDistance): # First point, so immediatly our new best
					currentClosest = waypoint
					currentClosestDistance = distance

		if currentClosest == None:
			make_response("No valid waiting positions!!", 400)
		
		else:
			robot.waypointName = currentClosest.name
			db.session.add(robot) # Set the bots new waypoint
			db.session.commit()

			jsonData = jsonify(
				xCoord = currentClosest.xCoordinate,
				yCoord = currentClosest.yCoordinate,
				directionZ = currentClosest.directionZ,
				directionW = currentClosest.directionW
			)

			return make_response(jsonData, 200) # Send a 200 code to the bot

class Robots(Resource):
	def get(self): # Return all the bots in the network and what theyre currently doing

		robots = models.Robot.query.all()

		dataList = [] # Will store the formatted output JSON data

		for robot in robots:
			waypoint = models.Waypoints.query.filter_by(name = robot.waypointName).first()
			
			data = "{'robotID' : '" + str(robot.robotID) + "', "
			data = data + "'name' : '" + str(robot.name) + "', "
			data = data + "'model' : '" + str(robot.model) + "', "
			data = data + "'lastPing' : '" + str(robot.lastPing) + "', "
			data = data + "'stale' : '" + str(robot.stale) + "', " # Create the JSON object for this robot
			data = data + "'state' : '" + str(robot.state) + "', "

			data = data + "'waypointName : '" + str(robot.waypointName) + "', "

			if (waypoint is not None):
				data = data + "'waypointName' : '" + str(waypoint.name) + "', "
				data = data + "'xCoord' : '" + str(waypoint.xCoord) + "', "
				data = data + "'yCoord' : '" + str(waypoint.yCoord) + "'}"
			
			else:
				data = data + "'waypointName' : 'N/A', "
				data = data + "'xCoord' : ''N/A'', "
				data = data + "'yCoord' : ''N/A''}"

			dataList.append(data) # add the robot data to the output list

		if (len(dataList) == 0):
			return make_response("{}", 200)

		count = 1 # Element count
		jsonString = "{" # Starting { for the Json data

		for item in dataList:
			jsonString = jsonString + '"' + str(count) + '" : "' + str(item) + '", ' # Construct the JSON line, including the robot data from before

			count += 1

		jsonString = jsonString[:-2] + "}" # Remove the final ", " and add a closing }

		response = make_response(jsonString, 200)
		response.mimetype = "text/plain"

		return response


initialisation()

api.add_resource(BotState, "/API/State") # Robot can extract and update its own state
api.add_resource(Ping, "/API/Ping") # Robots should ping the server the register their concurrency
api.add_resource(RegisterBot, "/API/Register") # Register a bot at spin up
api.add_resource(Robots, "/API/GetAllBots") # Fetch a list of all bots on the network
api.add_resource(Waypoint, "/API/Waypoint") # Add or delete waypoints
api.add_resource(WaitingWaypoint, "/API/WaitingWaypoint")
api.add_resource(ClientRequest, "/API/Client") # Client can request bots from the network

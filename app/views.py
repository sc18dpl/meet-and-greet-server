from flask import Flask, request, jsonify, make_response, render_template, redirect, url_for, request
from turbo_flask import Turbo

from app import app, db, api, models

from datetime import datetime

import json
import requests

import time
import threading

turbo = Turbo(app)

@app.before_first_request
def before_first_request():
    threading.Thread(target=update_load).start()

def update_load():
    with app.app_context():
        while True:
            time.sleep(1)
            turbo.push(turbo.replace(render_template("LoadData.html"), "load"))

@app.context_processor
def bots_load():
    robots = models.Robot.query.all()
    botData = []
    for robot in robots:
        botData.append(robot)

    waypoints = models.Waypoints.query.all()
    waypointData = []
    for waypoint in waypoints:
        waypointData.append(waypoint)

    return {"BotData" : botData, "WaypointData" : waypointData}

@app.route("/monitor", methods=["GET", "POST"])
def monitor():
    return render_template(
        "/monitor.html"
    )

@app.route("/clientrequest", methods=["GET", "POST"])
def clientRequestGet():
    url = str(request.host_url) + str("API/Client")
    waypoints = models.Waypoints.query.filter_by(waiting=False).all() # We don't want to show waiting zones to the user

    resultingWaypoints = []
    for waypoint in waypoints:
        if waypoint.name[:2] != "__": # Remove temporary waypoints from the data
            resultingWaypoints.append(waypoint)

    if request.method == "POST":
        clientLocation = request.form.getlist("ClientLocationRadio")
        targetLocations = request.form.getlist("ClientRequestCheck")
        clientName = request.form["clientName"]

        if (clientName == "" or clientName == None):
            return render_template(
                "/ClientRequest.html",
                botInformation = "Please enter your name",
                waypoints = resultingWaypoints
            )

        if len(clientLocation) == 0 or len(targetLocations) == 0:
            return render_template(
                "/ClientRequest.html",
                botInformation = "Please select atleast 1 client and target location",
                waypoints = resultingWaypoints
            )

        try:
            clientLocation = clientLocation[0]
            targetLocations.remove(clientLocation) # Remove the client location from the targets if the user has selected it
        except:
            pass

        if len(targetLocations) == 0: # Perform the same check after removing the client location
            return render_template(
                "/ClientRequest.html",
                botInformation = "Please select atleast 1 client and target (non-client) location",
                waypoints = resultingWaypoints
            )

        clientLocationWaypoint = models.Waypoints.query.filter_by(name=clientLocation).first()

        jsonData = {}
        jsonData["clientXCoord"] = clientLocationWaypoint.xCoordinate,
        jsonData["clientYCoord"] = clientLocationWaypoint.yCoordinate

        count = 1

        for location in targetLocations:
            jsonData["targetWaypointName" + str(count)] = location
            count += 1

        jsonData["clientName"] = clientName

        response = requests.get(url, params=jsonData)

        botInformation = None

        if response.status_code == 400:
            botInformation = "No bots are available to take your request. Please try again later!"

        elif response.status_code == 401:
            botInformation = "A waypoint was incorrect. Please refresh the page and try again"

        elif response.status_code == 200:
            jsonData = response.json()

            botInformation = "You have been assigned a turtlebot-" + str(jsonData["robotModel"]) + " named: " + str(jsonData["robotName"])
            botInformation = botInformation + ". When the bot arrives, please press the B0 button on the back of the robot to begin the navigation. "
            botInformation = botInformation + "If you have requested multiple waypoints, B0 must be pressed after stopping at each location to continue to the next waypoint"

        return render_template (
            "/ClientRequest.html",
            botInformation = botInformation,
            waypoints = resultingWaypoints
        )

    return render_template (
        "/ClientRequest.html",
        waypoints = resultingWaypoints
    )

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.mutable import MutableList

from app import db

class Robot(db.Model): # This model should be cleared when the server spins up
	__tablename__ = "Robot"

	robotID = db.Column(db.Integer, primary_key=True)

	name = db.Column(db.String(256), unique=True, nullable=False) # What name has this bot been assigned by the initialiser?
	model = db.Column(db.Integer, unique=False, nullable=False) # what Turtlebot model is this bot 2 or 3 or perhaps Lucie? The system should work without knowing this, but doesn't hurt to store it

	lastPing = db.Column(db.DateTime, unique=False, nullable=False) # What did the last ping come in
	stale = db.Column(db.Boolean, unique=False, nullable=False) # Has the bot maintained connection
	staleCount = db.Column(db.Integer, unique=False, nullable=False) # How many cycles has the bot been stale

	state = db.Column(db.String, unique=False, nullable=False) # What should the bot be currently doing
	waypointName = db.Column(db.String, unique=False, nullable=False) # Current waypoint that the bot should be heading towards
	targetWaypointList = db.Column(MutableList.as_mutable(db.PickleType), unique=False, nullable=True) # List of waypoints for the bot to travel to

	clientName = db.Column(db.String, unique=False, nullable=True)

	xCoordinate = db.Column(db.Float, unique=False, nullable=False) # Bots coordinates. To be updated
	yCoordinate = db.Column(db.Float, unique=False, nullable=False) # regularly by the Ping endpoint

	timeout = db.Column(db.DateTime, unique=False, nullable=True) # How long should the bot wait for the user until its freed up again?

	def __repr__(self):
		return "%s : (robotID = %s, state = %s, stale = %s, waypointName = %s, timeout = %s)" % (self.__tablename__, self.robotID, self.state, self.stale, self.waypointName, self.timeout)

class Waypoints(db.Model): # This model should remain persistant
	__tablename__ = "Waypoints"
	
	waypointID = db.Column(db.Integer, primary_key=True)

	name = db.Column(db.String, unique=True, nullable=False) # What name has been assigned to this waypoint?

	waiting = db.Column(db.Boolean, unique=False, nullable=False) # Is this a waiting position or not

	xCoordinate = db.Column(db.Float, unique=False, nullable=False) # X coordinate of the waypoint
	yCoordinate = db.Column(db.Float, unique=False, nullable=False) # Y coordinate of the waypoint
	directionZ = db.Column(db.Float, unique=False, nullable=False) # Direction of the waypoint (Typically gonna be 1 for non-waiting zones). Waiting zones will point towards the QR Code
	directionW = db.Column(db.Float, unique=False, nullable=False)

	def __repr__(self):
		return "%s : (waypointID = %s, name = %s, xCoordinate = %s, yCoordinate = %s)" % (self.__tablename__, str(self.waypointID), str(self.name), str(self.xCoordinate), str(self.yCoordinate))